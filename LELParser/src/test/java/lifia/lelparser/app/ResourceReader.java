package lifia.lelparser.app;

import lifia.lelparser.model.userStory.UserStory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author rodrigo.testa on 2019-12-04
 */
public class ResourceReader {

    public static List<UserStory> read(String name) throws IOException, URISyntaxException {
        List<UserStory> result;
        URL resource = NLServiceTest.class.getResource(name);

        Stream<String> stream = Files.lines(Paths.get(resource.toURI()));
        AtomicInteger i = new AtomicInteger(0);

        try {

            result=stream
                    .filter(s -> !s.startsWith("#"))
                    .filter(s -> !s.isEmpty())
                    .map(line -> { 	return new UserStory("US#" + i.incrementAndGet(), line);})
                    .collect(Collectors.toList());
        } finally {
            stream.close();
        }
        return result;
    }
}
