package lifia.lelparser.app;

import lifia.lelparser.model.PhraseStructure;
import lifia.lelparser.model.userStory.SpanishUserStoryParser;
import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.userStory.UserStoryParser;
import org.junit.Test;
import org.junit.Assert;

import java.util.Map;

/**
 * @author rodrigo.testa on 2019-12-04
 */
public class SpanishUserStoryParserTest {

    private static final String USER_STORY_1 = "Como vendedor, quiero registrar los productos y cantidades que me solicita un cliente para crear un pedido de venta.";
    private static final String USER_STORY_2 = "Como vendedor, quiero registrar los productos y cantidades que me solicita un cliente.";

    private UserStoryParser spanishUserStoryParser = new SpanishUserStoryParser();

    @Test
    public void testParseWithFullPartUserStoryParts(){

        UserStory userStory = new UserStory("#1", USER_STORY_1);

        Map<String, PhraseStructure> result = this.spanishUserStoryParser.parse(userStory);

        Assert.assertNotNull(result);

        Assert.assertEquals(3, result.size());

        result.forEach((key, value)-> Assert.assertNotNull(value));

    }

    @Test
    public void testParseWithoutSoPart(){

        UserStory userStory = new UserStory("#2", USER_STORY_2);

        Map<String, PhraseStructure> result = this.spanishUserStoryParser.parse(userStory);

        Assert.assertNotNull(result);

        Assert.assertEquals(2, result.size());

        result.forEach((key, value)-> Assert.assertNotNull(value));

    }
}
