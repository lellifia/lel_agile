package lifia.lelparser.app;

import lifia.lelparser.model.PhraseStructure;
import lifia.lelparser.model.userStory.EnglishUserStoryParser;
import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.userStory.UserStoryParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * @author rodrigo.testa on 2019-12-04
 */
public class EnglishUserStoryParserTest {

    private static final String USER_STORY_1 = "As someone who has posted an ad that is about to expire, seven days before it expires I want to be emailed a reminder so that I can go extend the ad. (Note: This means the ad could have an expiration date 37 days into the future, which is fine.)";
    private static final String USER_STORY_2 = "As an ETL-Developer, I want to define new database dialects and change the behaviour of existing ones more easily without Java coding";

    private UserStoryParser englishUserStoryParser = new EnglishUserStoryParser();

    @Test
    public void testParseWithFullPartUserStoryParts(){

        UserStory userStory = new UserStory("#1", USER_STORY_1);

        Map<String, PhraseStructure> result = this.englishUserStoryParser.parse(userStory);

        Assert.assertNotNull(result);

        Assert.assertEquals(3, result.size());

        result.forEach((key, value)-> Assert.assertNotNull(value));

    }

    @Test
    public void testParseWithoutSoPart(){

        UserStory userStory = new UserStory("#2", USER_STORY_2);

        Map<String, PhraseStructure> result = this.englishUserStoryParser.parse(userStory);

        Assert.assertNotNull(result);

        Assert.assertEquals(2, result.size());

        result.forEach((key, value)-> Assert.assertNotNull(value));

    }
}
