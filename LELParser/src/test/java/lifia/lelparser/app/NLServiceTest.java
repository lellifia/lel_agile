package lifia.lelparser.app;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lifia.lelparser.model.NLService;
import org.junit.Test;
import org.junit.Assert;
import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import lifia.lelparser.model.parserResults.ResultFormatter;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class NLServiceTest {
	@Test
	public void testParser() throws  IOException {
		// Creates a new NLP
		NLService service = new NLService();

		// Parse to map (sample phrase)
		ParserResult result = service.processUserStories(NLServiceTest.buildSampleUserStories(),"en");

		Assert.assertNotNull(result);
		Assert.assertFalse(result.getContent().isEmpty());
		Assert.assertFalse(result.getUserStories().isEmpty());

		// Print the result
		ResultFormatter.dumpToHTML(result);
	}

	private static Collection<UserStory> buildSampleUserStories() {
		ArrayList<UserStory> stories = new ArrayList<>();

		stories.add(new UserStory("US#1",
				"As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"));
		stories.add(new UserStory("US#2",
				"As a moderator I want to invite estimators by given them a url where they can access the game So that we can start the game"));
		stories.add(new UserStory("US#3",
				"As an estimator I want to join a game by entering my name on the page I received the url for So that I can participate"));
		stories.add(new UserStory("US#4",
				"As a moderator I want to start a round by entering an item in a single multi-line text field So that we can estimate it"));
		stories.add(new UserStory("US#5",
				"As an estimator I want to see the item we are estimating So that we can estimate it"));
		stories.add(new UserStory("US#6",
				"As a moderator I want to edit an item in the list of items to be estimated So that I can make it better reflect the team’s understanding of the item"));
		stories.add(new UserStory("US#7",
				"As a moderator I want to export a transcript of a game as a CSV file So that I can further process the stories and estimates"));

		return stories;
	}

	
	@Test

	public void testParserPentaho() throws IOException, URISyntaxException {
		// Creates a new NLP
		NLService service = new NLService();

		Collection<UserStory> userStories;
		userStories = ResourceReader.read("/pentaho.txt");
		
		
		// Parse to map (sample phrase)

		ParserResult result = service.processUserStories(userStories,"en");
		Assert.assertNotNull(result);
		Assert.assertFalse(result.getContent().isEmpty());
		Assert.assertFalse(result.getUserStories().isEmpty());

		// Print the result
		ResultFormatter.dumpToHTML(result);
	}
	
	
	@Test

	public void testParserMountainGoat() throws IOException, URISyntaxException {
		// Creates a new NLP
		NLService service = new NLService();

		// Parse to map (sample phrase)
		ParserResult result = service.processUserStories(NLServiceTest.readMountainGoatUS(), "en");

		Assert.assertNotNull(result);
		Assert.assertFalse(result.getContent().isEmpty());
		Assert.assertFalse(result.getUserStories().isEmpty());

		// Print the result
		ResultFormatter.dumpToHTML(result);
	}

	@Test
	public void testSpanishUserStories() throws IOException, URISyntaxException {
		// Creates a new NLP
		NLService service = new NLService();

		// Parse to map (sample phrase)
		List<UserStory> userStories = ResourceReader.read("/spanish_user_stories.txt");

		ParserResult result = service.processUserStories(userStories, "es");

		Assert.assertNotNull(result);
		Assert.assertFalse(result.getContent().isEmpty());
		Assert.assertFalse(result.getUserStories().isEmpty());

		// Print the result
		ResultFormatter.dumpToHTML(result);
	}

	private static Collection<UserStory> readMountainGoatUS() throws IOException, URISyntaxException {

		String name = "/mountaingoat.txt";
		List<UserStory> result;
		result = ResourceReader.read(name);
		
		return result;
	}


}
