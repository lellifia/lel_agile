package ar.edu.info.lifia.lelparser.services;

import io.swagger.annotations.ApiParam;
import lifia.lelparser.exceptions.InvalidRequirementFormatException;
import lifia.lelparser.model.NLService;
import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.parserResults.ParserResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Locale;

@RestController
public class UseCaseParserController {
    static final String EXAMPLE_REQUEST = "[{"
            + "\"id\": \"string\"," +
            " \"text\": \"As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators\"";


    private NLService nlService = new NLService();

    @RequestMapping(value = "/parser", method = RequestMethod.POST)
    public ParserResult parseUserStory(@ApiParam(defaultValue = EXAMPLE_REQUEST) @RequestBody List<UserStory> userStoryList, @RequestHeader("accept-language") String acceptLanguage) throws InvalidRequirementFormatException {

        String language = Locale.LanguageRange.parse(acceptLanguage).get(0).getRange().split("-")[0];
        // Parse to map (sample phrase)
        return this.nlService.processUserStories(userStoryList, language);

    }
}