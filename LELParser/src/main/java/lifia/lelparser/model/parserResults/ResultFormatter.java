package lifia.lelparser.model.parserResults;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.phraseContent.PhraseContent;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class ResultFormatter {

    public static void dumpToConsole(ParserResult result) {
        System.out.println("**********************************************************");
        for (PhraseContent currentContent : result.getContent()) {
            System.out.println("Valor literal: '" + currentContent.getValue() + "'");
            System.out.println("Entidad tipo: " + currentContent.getType());
            System.out.println("Cantidad de apariciones: " + currentContent.getAppearances());
            System.out.println("Referenciada en la historia de usuario: ");
            currentContent.getUsReferences().stream().forEach((currUS) -> {
                System.out.println("     " + currUS.getId());
            });
            System.out.println("**********************************************************");
        }
    }

    public static void dumpToHTML(ParserResult result) throws IOException {
        // Load the HTML template
        String pathname = "/template.html";

        String htmlString = getTemplate(pathname);
        
        

        String title = "NL Parser | Index";

        String body = "<h3><p class='bg-primary'>NL Parser | Index of generated results</p></h3>";

        body = body.concat("<ul class='list-group'>");

        body = body.concat("<li class='list-group-item'><a href='userStories.html'>User stories</a></li>");
        body = body.concat("<li class='list-group-item'><a href='subjects.html'>Subjects</a></li>");
        body = body.concat("<li class='list-group-item'><a href='objects.html'>Objects</a></li>");
        body = body.concat("<li class='list-group-item'><a href='verbs.html'>Verbs</a></li>");

        body = body.concat("</ul>");

        body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        
        
        htmlString = htmlString.replace("$title", title);
        htmlString = htmlString.replace("$body", body);
        File newHtmlFile = new File("generated/index.html");
        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        
        
        // ********************************************************        

        // Generate user stories HTML *****************************
        htmlString = getTemplate(pathname);

        title = "NL Parser | User Stories";

        body = "<h3><p class='bg-primary'>User stories</p></h3><ul class='list-group'>";

        for (UserStory uStory : result.getUserStories()) {
            body = body.concat("<li class='list-group-item'>");
            body = body.concat("<b>" + uStory.getId() + "</b>");
            body = body.concat("<br/>" + uStory.getText());
            body = body.concat("</li>");
            body = body.concat("<br/>");
        }
        body = body.concat("</ul>");
        
        body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        

        htmlString = htmlString.replace("$title", title);
        htmlString = htmlString.replace("$body", body);
        newHtmlFile = new File("generated/userStories.html");
        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        // ********************************************************

        // Generate US details HTML *****************************
        for (UserStory currStory : result.getUserStories()) {
            htmlString = getTemplate(pathname);


            title = "NL Parser | User Story " + currStory.getId() + " details";

            body = "<h3><p class='bg-primary'>User Story details:</p></h3><ul class='list-group'>";
            body = body.concat("<li class='list-group-item'>");
            body = body.concat("<b>ID:</b> " + currStory.getId());
            body = body.concat("</li>");
            body = body.concat("<li class='list-group-item'>");
            body = body.concat("<b>Text:</b> " + currStory.getText());
            body = body.concat("</li>");
            body = body.concat("<br/>");

            body = body.concat("</ul>");
            
            body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        

            htmlString = htmlString.replace("$title", title);
            htmlString = htmlString.replace("$body", body);
            String usFileName = currStory.getId().substring(currStory.getId().indexOf("US#") + 3);
            newHtmlFile = new File("generated/us" + usFileName + ".html");

            FileUtils.writeStringToFile(newHtmlFile, htmlString);
        }
        // ********************************************************

        // Generate subjects HTML *****************************
        htmlString = getTemplate(pathname);

        title = "NL Parser | Subjects";

        body = "<h3><p class='bg-primary'>Subjects:</p></h3><ul class='list-group'>";
        for (PhraseContent currContent : result.getContent()) {
            if (currContent.getType().equals("subject")) {
                body = body.concat("<li class='list-group-item'>");
                body = body.concat("<b>Value:</b> " + currContent.getValue());
                body = body.concat("<span class='badge'>" + currContent.getAppearances() + " appearances</span>");
                body = body.concat("<br/><b>Referenced in:</b><br/>");

                for (UserStory uStory : currContent.getUsReferences()) {
                    String usURL = "us" + uStory.getId().substring(uStory.getId().indexOf("US#") + 3) + ".html";
                    body = body.concat("| <a href='" + usURL + "'>" + uStory.getId() + "</a> ");
                }

                body = body.concat("|</li>");
                body = body.concat("<br/>");
            }
        }

        body = body.concat("</ul>");
        
        body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        

        htmlString = htmlString.replace("$title", title);
        htmlString = htmlString.replace("$body", body);
        newHtmlFile = new File("generated/subjects.html");

        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        // ********************************************************

        // Generate verbs HTML *****************************
        htmlString = getTemplate(pathname);;

        title = "NL Parser | Verbs";

        body = "<h3><p class='bg-primary'>Verbs:</p></h3><ul class='list-group'>";
        for (PhraseContent currContent : result.getContent()) {
            if (currContent.getType().equals("verb")) {
                body = body.concat("<li class='list-group-item'>");
                body = body.concat("<b>Value:</b> " + currContent.getValue());
                body = body.concat("<span class='badge'>" + currContent.getAppearances() + " appearances</span>");
                body = body.concat("<br/><b>Referenced in:</b><br/>");
                for (UserStory uStory : currContent.getUsReferences()) {
                    String usURL = "us" + uStory.getId().substring(uStory.getId().indexOf("US#") + 3) + ".html";
                    body = body.concat("| <a href='" + usURL + "'>" + uStory.getId() + "</a> ");
                }
                body = body.concat("|</li>");
                body = body.concat("<br/>");
            }
        }

        body = body.concat("</ul>");
        
        body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        

        htmlString = htmlString.replace("$title", title);
        htmlString = htmlString.replace("$body", body);
        newHtmlFile = new File("generated/verbs.html");

        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        // ********************************************************

        // Generate objects HTML *****************************
        htmlString = getTemplate(pathname);

        title = "NL Parser | Objects";

        body = "<h3><p class='bg-primary'>Objects:</p></h3><ul class='list-group'>";
        for (PhraseContent currContent : result.getContent()) {
            if (currContent.getType().equals("object")) {
                body = body.concat("<li class='list-group-item'>");
                body = body.concat("<b>Value:</b> " + currContent.getValue());
                body = body.concat("<span class='badge'>" + currContent.getAppearances() + " appearances</span>");
                body = body.concat("<br/><b>Referenced in:</b><br/>");
                for (UserStory uStory : currContent.getUsReferences()) {
                    String usURL = "us" + uStory.getId().substring(uStory.getId().indexOf("US#") + 3) + ".html";
                    body = body.concat("| <a href='" + usURL + "'>" + uStory.getId() + "</a> ");
                }
                body = body.concat("|</li>");
                body = body.concat("<br/>");
            }
        }

        body = body.concat("</ul>");
        
        body = body.concat("<footer><div class='container'><p class='text-muted'>NL Parser | Antonelli, L., Urbieta, M, Toneli, J. | 2016</p></div></footer>");        
        
        htmlString = htmlString.replace("$title", title);
        htmlString = htmlString.replace("$body", body);
        newHtmlFile = new File("generated/objects.html");

        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        // ********************************************************

    }

	private static String getTemplate(String pathname) throws IOException {
		// Generate index HTML *****************************
        InputStream resourceAsStream = ResultFormatter.class.getResourceAsStream(pathname);
        
        StringWriter writer = new StringWriter();
        IOUtils.copy(resourceAsStream, writer);
        String htmlString = writer.toString();
		return htmlString;
	}

}
