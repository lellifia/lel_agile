package lifia.lelparser.model.parserResults;

import java.util.ArrayList;
import java.util.Collection;

import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.phraseContent.PhraseContent;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class ParserResult {

    private Collection<UserStory> userStories;
    protected Collection<PhraseContent> content;

    public ParserResult() {
        this.userStories = new ArrayList<>();
        this.content = new ArrayList<>();
    }

    /**
     * @return the content
     */
    public Collection<PhraseContent> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(Collection<PhraseContent> content) {
        this.content = content;
    }

    public void addContents(Collection<PhraseContent> contents) {
        contents.forEach(this::addOrUpdateSinglePhraseContent);
    }

    private void addOrUpdateSinglePhraseContent(PhraseContent nuevoCont) {


        for (PhraseContent currContent : this.getContent()) {
                if (currContent.equals(nuevoCont)) {
                    currContent.setAppearances(currContent.getAppearances() + 1);
                    currContent.getUsReferences().addAll(nuevoCont.getUsReferences());
                    return;
                }
        }

        this.getContent().add(nuevoCont);


    }

    /**
     * @return the userStories
     */
    public Collection<UserStory> getUserStories() {
        return userStories;
    }

    /**
     * @param userStories the userStories to set
     */
    public void setUserStories(Collection<UserStory> userStories) {
        this.userStories = userStories;
    }

}
