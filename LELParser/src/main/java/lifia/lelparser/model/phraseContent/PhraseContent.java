package lifia.lelparser.model.phraseContent;

import java.util.Collection;
import java.util.Objects;
import lifia.lelparser.model.userStory.UserStory;

/**
 * Represents a single content in a phrase.
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class PhraseContent {

    private String type;
    private String value;
    private Collection<UserStory> usReferences;
    private int appearances;

    public PhraseContent(String type, String value, Collection<UserStory> usReferences) {
        this.type = type;
        this.value = value;
        this.usReferences = usReferences;
        this.appearances = 1;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }


    /**
     * @return the usReference
     */
    public Collection<UserStory> getUsReferences() {
        return usReferences;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.type);
        hash = 19 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhraseContent other = (PhraseContent) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the appearances
     */
    public int getAppearances() {
        return appearances;
    }

    /**
     * @param appearances the appearances to set
     */
    public void setAppearances(int appearances) {
        this.appearances = appearances;
    }

}
