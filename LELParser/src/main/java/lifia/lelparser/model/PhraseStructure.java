package lifia.lelparser.model;

import lifia.lelparser.model.phraseContent.PhraseContent;
import lifia.lelparser.model.userStory.UserStory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the contents of a phrase (subjets, verbs, etc.)
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class PhraseStructure {

    private List<PhraseContent> phraseContents;

    public PhraseStructure() {
        this.phraseContents = new ArrayList<>();
    }

    /**
     * @return the subjects
     */
    public List<PhraseContent> getSubjects() {

        return this.phraseContents.stream()
                .filter(currentContent -> (currentContent.getType().equals("subject")))
                .collect(Collectors.toList());

    }

    /**
     * @return the verbs
     */
    public List<PhraseContent> getVerbs() {

        return this.phraseContents.stream()
                .filter(currentContent -> (currentContent.getType().equals("verb")))
                .collect(Collectors.toList());

    }

    public void addSubject(String newSubject, UserStory usRef) {
        this.phraseContents.add(new PhraseContent("subject", newSubject, new ArrayList<>(Arrays.asList(usRef))));
    }

    public void addVerb(String newVerb, UserStory usRef) {
        this.phraseContents.add(new PhraseContent("verb", newVerb,new ArrayList<>(Arrays.asList(usRef))));
    }

    public void addObject(String newObj, UserStory usRef) {
        this.phraseContents.add(new PhraseContent("object", newObj, new ArrayList<>(Arrays.asList(usRef))));
    }

    /**
     * @return the objects
     */
    public List<PhraseContent> getObjects() {

        return this.phraseContents.stream()
                .filter(currentContent -> (currentContent.getType().equals("object")))
                .collect(Collectors.toList());

    }

}
