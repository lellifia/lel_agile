package lifia.lelparser.model.phraseParser;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;

import java.io.StringReader;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author rodrigo.testa on 2019-11-26
 */
public abstract class AbstractPhraseParser {

    private LexicalizedParser lexicalizedParser;

    AbstractPhraseParser(String parserModel) {
        // Load the default lexicalized parser with the parser model
        this.lexicalizedParser = LexicalizedParser.loadModel(parserModel);
    }

    /**
     * @return the localizedParser
     */
    LexicalizedParser getLexicalizedParser() {
        return lexicalizedParser;
    }


    /**
     * Parse a given string with the configured model and parser
     *
     * @param textToBeParsed the String to be parsed
     * @return Tree with the parsed result
     */
    private Tree analize(String textToBeParsed) {
        TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
        Tokenizer<CoreLabel> tokenizerLabel = tokenizerFactory.getTokenizer(new StringReader(textToBeParsed));
        return this.processWithTreebank(this.getLexicalizedParser().apply(tokenizerLabel.tokenize()));
    }

    /**
     *
     * @param tree a Tree from lexicalized parser
     * @return a tree with treebank applied
     */
    protected abstract Tree processWithTreebank(Tree tree);


    /**
     *
     * @param id a string
     * @return a code linked to given ID
     */
    protected abstract Set<String> getCodesFor(String id);


    /**
     *
     * @param phrase to process
     * @return a list of strings with the subjects from given phrase
     */
    public List<String> getSubjects(String phrase) {
        return this.getWordsFor(phrase, "subject");
    }

    /**
     *
     * @param phrase to process
     * @return a list of strings with the verbs from given phrase
     */
    public List<String> getVerbs(String phrase) {
        return this.getWordsFor(phrase, "verb");
    }


    /**
     *
     * @param phrase the string to process
     * @param id an string ID, it can be verb or subject
     * @return a List with String that contains words with the codes from given ID
     */
    private List<String> getWordsFor(String phrase, String id) {
        return this.analize(phrase).taggedYield()
                .stream()
                .filter(item -> this.getCodesFor(id).contains(item.tag()))
                .map(Word::word)
                .collect(Collectors.toList());
    }

}
