package lifia.lelparser.model.phraseParser;

import edu.stanford.nlp.trees.Tree;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author rodrigo.testa on 2019-11-25
 */
public class SpanishPhraseParser extends AbstractPhraseParser {

    private Map<String, Set<String>> phraseCodes = Stream.of(
            new AbstractMap.SimpleImmutableEntry<>("subject", new HashSet<>(Arrays.asList("nc0s000","nc0p000"))),
            new AbstractMap.SimpleImmutableEntry<>("verb", new HashSet<>(Arrays.asList("vmn0000", "vmsp000"))))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private static final String PARSER_MODEL = "edu/stanford/nlp/models/lexparser/spanishPCFG.ser.gz";

    public SpanishPhraseParser() {
        super(PARSER_MODEL);
    }

    @Override
    protected Tree processWithTreebank(Tree tree) {
        return tree;
    }

    @Override
    protected Set<String> getCodesFor(String id) {
        return this.phraseCodes.get(id);
    }

}
