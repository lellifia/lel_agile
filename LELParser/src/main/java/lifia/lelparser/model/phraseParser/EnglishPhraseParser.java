package lifia.lelparser.model.phraseParser;

import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author rodrigo.testa on 2019-11-25
 */
public class EnglishPhraseParser extends AbstractPhraseParser {


    private Map<String, Set<String>> phraseCodes = Stream.of(
            new AbstractMap.SimpleImmutableEntry<>("subject", new HashSet<>(Collections.singletonList("NN"))),
            new AbstractMap.SimpleImmutableEntry<>("verb", new HashSet<>(Collections.singletonList("VB"))))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private static final String PARSER_MODEL = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";

    public EnglishPhraseParser() {

        super(PARSER_MODEL);
    }

    @Override
    protected Tree processWithTreebank(Tree parse) {
        TreebankLanguagePack tlp = this.getLexicalizedParser().treebankLanguagePack(); // PennTreebankLanguagePack for English
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        gs.typedDependenciesCCprocessed();
        return parse;
    }

    @Override
    protected Set<String> getCodesFor(String id) {
        return this.phraseCodes.get(id);
    }

}
