package lifia.lelparser.model;

import lifia.lelparser.exceptions.ParserNotFoundException;
import lifia.lelparser.model.parserResults.ParserResult;
import lifia.lelparser.model.userStory.EnglishUserStoryParser;
import lifia.lelparser.model.userStory.SpanishUserStoryParser;
import lifia.lelparser.model.userStory.UserStory;
import lifia.lelparser.model.userStory.UserStoryParser;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author rodrigo.testa on 2019-11-25
 */
public class NLService {


    private Map<String, UserStoryParser> userStoryParserMap = Stream.of(
            new AbstractMap.SimpleImmutableEntry<>("en", new EnglishUserStoryParser()),
            new AbstractMap.SimpleImmutableEntry<>("es", new SpanishUserStoryParser()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    /**
     * Parses a collection of user stories.
     *
     * @param userStories the user stories to parse.
     * @return the resulting data of the parsing.
     */
    public ParserResult processUserStories(Collection<UserStory> userStories, String language) {
        ParserResult result = new ParserResult();
        result.setUserStories(userStories);
        UserStoryParser parser = Optional.ofNullable(this.userStoryParserMap.get(language))
                .orElseThrow(() -> new ParserNotFoundException(String.format("No parser found for language %s", language)));


        userStories.forEach(item -> {
            try {
                parser.parse(item).forEach((key, value) -> {
                    result.addContents(value.getSubjects());
                    result.addContents(value.getObjects());
                    result.addContents(value.getVerbs());
                });
            } catch (Exception e) {
                System.out.println(String.format("There is a problem with the following US: %s. Cause: %s", item.getText(), e.getCause()));
            }
        });

        return result;

    }
}
