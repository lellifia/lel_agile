package lifia.lelparser.model.userStory;

import lifia.lelparser.model.phraseParser.AbstractPhraseParser;
import lifia.lelparser.model.phraseParser.SpanishPhraseParser;

/**
 * @author rodrigo.testa on 2019-11-28
 */
public class SpanishUserStoryParser extends UserStoryParser {

    private static final String[] SO_THAT = new String[]{"para"};
    private static final String[] I_WANT_TO = new String[]{"quiero"};
    private static final String AS = "como";

    private AbstractPhraseParser phraseParser = new SpanishPhraseParser();

    public SpanishUserStoryParser() {
        super(AS, I_WANT_TO, SO_THAT);
    }

    @Override
    protected AbstractPhraseParser getPhraseParser() {
        return this.phraseParser;
    }
}
