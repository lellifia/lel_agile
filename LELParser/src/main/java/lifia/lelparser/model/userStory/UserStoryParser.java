package lifia.lelparser.model.userStory;

import edu.stanford.nlp.util.Pair;
import lifia.lelparser.model.PhraseStructure;
import lifia.lelparser.model.phraseParser.AbstractPhraseParser;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author rodrigo.testa on 2019-11-28
 */
public abstract class UserStoryParser {

    private String[] soThat;
    private String[] iWantTo;
    private String as;


    UserStoryParser(@NotNull String as, @NotNull String[] iWantTo, @NotNull String[] soThat) {
        this.as = as;
        this.iWantTo = iWantTo;
        this.soThat = soThat;
    }

    public Map<String, PhraseStructure> parse(UserStory userStory) {
        Map<String, PhraseStructure> result = new HashMap<>();

        // First part ("AS")
        String as = this.getAs(userStory.getText().toLowerCase());
        result.put(as, this.parseContent(as, userStory, false));

        // Second part ("I WANT TO")
        String iWantTo = this.getIWantTo(userStory.getText().toLowerCase());
        result.put(iWantTo, this.parseContent(iWantTo, userStory, true));

        // Third part ("SO THAT")
        if (this.hasSoThatSection(userStory.getText())) {
            String soThat = this.getSo(userStory.getText().toLowerCase());
            result.put(soThat, this.parseContent(soThat, userStory, false));
        }

        return result;
    }

    protected abstract AbstractPhraseParser getPhraseParser();

    private PhraseStructure parseContent(String phrase, UserStory usToBeParsed, boolean treatAsNotionSection) {

        // The contents of the phrase
        PhraseStructure pContents = new PhraseStructure();

        // Parse subjects
        this.getPhraseParser().getSubjects(phrase).forEach(item -> {
            if (treatAsNotionSection) {
                pContents.addObject(item, usToBeParsed);
            } else {
                pContents.addSubject(item, usToBeParsed);
            }
        });


        // Parse actions
        this.getPhraseParser().getVerbs(phrase).forEach(item -> pContents.addVerb(item, usToBeParsed));

        return pContents;
    }



    private String getAs(String text) {

        int indexAS = text.indexOf(this.as);

        Pair<String, Integer> iwtPair = lookup(text, this.iWantTo);

        return text.substring(indexAS + this.as.length() + 1, (iwtPair != null) ? iwtPair.second() : text.length());
    }

    private String getIWantTo(String text) {

        Pair<String, Integer> iwtPair = lookup(text, this.iWantTo);
        Pair<String, Integer> stPair = lookup(text, this.soThat);

        return text.substring(iwtPair.second + iwtPair.first().length() + 1, (stPair != null) ? stPair.second : text.length());
    }

    private String getSo(String text) {

        Pair<String, Integer> stPair = lookup(text, this.soThat);

        //Fix because it doesnt have SO_THAT
        if (stPair == null) {
            return null;
        }

        return text.substring(stPair.second + stPair.first.length() + 1);

    }

    private boolean hasSoThatSection(String text) {
        return lookup(text, this.soThat) != null;
    }

    private Pair<String, Integer> lookup(String body, String[] param) {

        return Stream.of(param)
                .filter(item -> body.toLowerCase().contains(item.toLowerCase()))
                .findFirst()
                .map(item ->  new Pair<>(item, body.toLowerCase().indexOf(item.toLowerCase())))
                .orElse(null);

    }
}
