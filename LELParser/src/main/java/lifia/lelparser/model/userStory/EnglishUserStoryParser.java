package lifia.lelparser.model.userStory;

import lifia.lelparser.model.phraseParser.AbstractPhraseParser;
import lifia.lelparser.model.phraseParser.EnglishPhraseParser;

/**
 * @author rodrigo.testa on 2019-11-28
 */
public class EnglishUserStoryParser extends UserStoryParser {

    private static final String[] SO_THAT = new String[]{"so that", "that is"};
    private static final String[] I_WANT_TO = new String[]{"i want to", "i want", ","};
    private static final String AS = "as";

    private AbstractPhraseParser phraseParser = new EnglishPhraseParser();

    public EnglishUserStoryParser() {
        super(AS, I_WANT_TO, SO_THAT);
    }

    @Override
    protected AbstractPhraseParser getPhraseParser() {
        return this.phraseParser;
    }
}
