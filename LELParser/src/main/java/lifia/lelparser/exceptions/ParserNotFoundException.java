package lifia.lelparser.exceptions;

/**
 * @author rodrigo.testa on 2019-11-26
 */
public class ParserNotFoundException extends RuntimeException {

    public ParserNotFoundException(String message) {
        super(message);
    }
}
