
# README #

  - [About](#about)
  - [Set up](#set-up)
  - [Endpoints](#endpoints)
  - [Swagger](#swagger)

  

## About


LELParser is an application for find subjects, objects and verbs that contains an user story.

It uses the library [LexParser](https://nlp.stanford.edu/software/lex-parser.shtml) from Stanford

  
  

## Set up

  



-  **git clone https://bitbucket.org/lellifia/lel_agile.git**

-  **cd lel_agile**

-  **mvn spring-boot:run**

  

> It requires Maven and Java SDK installed.

  
  ## Endpoints

-  **Method**: POST

-  **URL**: /parser  

<p>For this endpoint, accept-language header is mandatory because we need to know which language will be use to process the user stories.</p>

Actually, two options are available:

- es
- en

Body examples:

English:

```
#!json

[
    {
        "id": "string",
        "text": "As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"
    }
]

```

``` 
curl -X POST -H "Accept-Language:en" -H "Content-Type:application/json" -d '[
    {
        "id": "string",
        "text": "As a moderator I want to create a new game by entering a name and an optional description So that I can start inviting estimators"
    }
]' http://localhost:8080/parser
```

Spanish:

```
#!json

[
    {
        "id": "string",
        "text": "Como usuario web quiero consultar la tabla de pedidos para saber el estado de todos mis pedidos"
    }
]

```

``` 
curl -X POST -H "Accept-Language:es" -H "Content-Type:application/json" -d '[
    {
        "id": "string",
        "text": "Como usuario web quiero consultar la tabla de pedidos para saber el estado de todos mis pedidos"
    }
]' http://localhost:8080/parser
```


## Swagger

The testing is quite strightforward, you need to open a browser pointing out to: [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html#!/use-case-parser-controller/)